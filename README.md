## ABM de Materias
Aplicación web que sirve para administrar materias. Creada con el framework PHP Codeigniter y el framework CSS Bulma.

![admin_inicio_panel](images/admin_inicio_panel.png)

![admin_materias](images/admin_materias.png)

#### Requisitos
- PHP
- MySQL
- Apache2

#### Descarga
```
git clone https://gitlab.com/ezefran/abm-materias.git
```
Luego mover los archivos dentro de `abm-materias` al directorio http de apache, para poder acceder a la web desde `localhost`.

#### Configurar base de datos de ejemplo
```
mysql -u root -p
mysql> source ./bd_demo/estructura.sql
mysql> source ./bd_demo/datos.sql
```

##### Usuarios de ejemplo
- Usuario: admin; Contraseña: asd123
- Usuario: alumno; Contraseña: asd123