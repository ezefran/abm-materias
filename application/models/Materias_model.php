<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Materias_model extends CI_Model {

    public function get_materia($id) {
        $select = 'materias.id, materias.nombre, materias.descripcion, materias.carga_horaria, carreras.nombre AS carrera';
        $this->db->select($select);
        $this->db->from('materias');
        $this->db->join('carreras', 'materias.carrera_id = carreras.id');
        $this->db->where('materias.id = '.$id);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Cantidades que sirven para la paginacion
    public function get_cantidades($materia, $items_por_pagina = 8) {
        $select = 'COUNT(materias.id) AS cantidad_materias, COUNT(materias.id)/'.$items_por_pagina.' AS cantidad_paginas';
        $this->db->select($select);
        $this->db->from('materias');
        $this->db->join('carreras', 'materias.carrera_id = carreras.id');
        $this->db->like('materias.nombre', $materia, 'after');
        $query = $this->db->get();
        $cantidades = $query->row_array();
        $cantidades['cantidad_paginas'] = ceil($cantidades['cantidad_paginas']);
        return $cantidades;
    }

    public function get_materias($materia, $pagina = 1, $items_por_pagina = 8) {
        $select = 'materias.id, materias.nombre, materias.descripcion, materias.carga_horaria, carreras.nombre AS carrera';
        $this->db->select($select);
        $this->db->from('materias');
        $this->db->join('carreras', 'materias.carrera_id = carreras.id');
        $this->db->like('materias.nombre', $materia, 'after');
        $this->db->order_by('carreras.nombre', 'ASC');
        $this->db->order_by('materias.nombre', 'ASC');
        if ($pagina != 0)
            $this->db->limit($items_por_pagina, ($pagina-1) * $items_por_pagina);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Busca las materias no aprobadas pertenecientes a la carrera del alumno
    // y verifica que, si tiene correlativas, el alumno las haya aprobado todas.
    public function get_materias_cursables($usuario_id) {
        $sql = "SELECT materias.* FROM materias
                JOIN carreras ON carreras.id = materias.carrera_id
                JOIN usuarios ON usuarios.carrera_id = carreras.id
                WHERE usuarios.id = ? AND materias.id NOT IN (
                    SELECT correlativas.materia_id FROM correlativas
                ) AND materias.id NOT IN (
                    SELECT materias.id FROM materias
                    JOIN cursadas ON cursadas.materia_id = materias.id
                    JOIN usuarios ON usuarios.id = cursadas.usuario_id
                    WHERE usuarios.id = ? AND cursadas.estado = 'APROBADA'
                )
                UNION DISTINCT
                SELECT materias.* FROM materias
                JOIN correlativas C ON C.materia_id = materias.id
                WHERE (
                    SELECT COUNT(*) FROM correlativas
                    WHERE correlativas.materia_id = C.materia_id
                ) = (
                    SELECT COUNT(*) FROM correlativas
                    JOIN materias ON materias.id = correlativas.correlativa_id
                    JOIN cursadas ON cursadas.materia_id = materias.id
                    JOIN usuarios ON usuarios.id = cursadas.usuario_id
                    WHERE correlativas.materia_id = C.materia_id AND cursadas.estado = 'APROBADA' AND usuarios.id = ?
                )";
        $query = $this->db->query($sql, array($usuario_id, $usuario_id, $usuario_id));
        return $query->result_array();
    }

    /*
     *    Funciones ABM
     */

    public function insert_materia($data) {
        $this->db->insert('materias', $data);
    }
    
    public function update_materia($data) {
        $this->db->update('materias', $data, array('id' => $data['id']));
    }

    public function delete_materia($id) {
        $this->db->delete('materias', array('id' => $id));
    }
}