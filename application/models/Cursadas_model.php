<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cursadas_model extends CI_Model {

    public function get_cursada($id) {
        $select = 'cursadas.id, materias.nombre AS m_nombre, usuarios.nombre AS u_nombre, usuarios.apellido AS u_apellido,
                cursadas.estado, cursadas.nota, cursadas.fecha, usuarios.id AS u_id, materias.id AS m_id';
        $this->db->select($select);
        $this->db->from('cursadas');
        $this->db->join('materias', 'cursadas.materia_id = materias.id');
        $this->db->join('usuarios', 'cursadas.usuario_id = usuarios.id');
        $this->db->where('cursadas.id = '.$id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_cantidades($materia, $items_por_pagina = 8) {
        $select = 'COUNT(cursadas.id) AS cantidad_cursadas, COUNT(cursadas.id)/'.$items_por_pagina.' AS cantidad_paginas';
        $this->db->select($select);
        $this->db->from('cursadas');
        $this->db->join('materias', 'cursadas.materia_id = materias.id');
        $this->db->join('usuarios', 'cursadas.usuario_id = usuarios.id');
        $this->db->like('materias.nombre', $materia, 'after');
        $query = $this->db->get();
        $cantidades = $query->row_array();
        $cantidades['cantidad_paginas'] = ceil($cantidades['cantidad_paginas']);
        return $cantidades;
    }

    public function get_cursadas($materia, $pagina = 1, $items_por_pagina = 8) {
        $select = 'cursadas.id, materias.nombre AS m_nombre, usuarios.nombre AS u_nombre, usuarios.apellido AS u_apellido,
                cursadas.estado, cursadas.nota, cursadas.fecha';
        $this->db->select($select);
        $this->db->from('cursadas');
        $this->db->join('materias', 'cursadas.materia_id = materias.id');
        $this->db->join('usuarios', 'cursadas.usuario_id = usuarios.id');
        $this->db->like('materias.nombre', $materia, 'after');
        $this->db->order_by('materias.nombre', 'ASC');
        $this->db->order_by('usuarios.nombre', 'ASC');
        if ($pagina != 0)
            $this->db->limit($items_por_pagina, ($pagina-1) * $items_por_pagina);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_historial($usuario_id, $estado) {
        $select = 'materias.nombre AS materia, carreras.nombre AS carrera, cursadas.estado, nota, fecha';
        $this->db->select($select);
        $this->db->from('cursadas');
        $this->db->join('materias', 'cursadas.materia_id = materias.id');
        $this->db->join('carreras', 'materias.carrera_id = carreras.id');
        $this->db->join('usuarios', 'cursadas.usuario_id = usuarios.id');
        $this->db->order_by('fecha', 'ASC');
        $this->db->where('usuarios.id', $usuario_id);
        $this->db->where('cursadas.estado', $estado);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*
     *    Funciones ABM
     */

    public function insert_cursada($data) {
        $data['nota'] = ($data['nota'] == '' ? NULL : $data['nota']);
        $data['fecha'] = ($data['fecha'] == '' ? NULL : $data['fecha']);
        $this->db->insert('cursadas', $data);
    }

    public function update_cursada($data) {
        $this->db->update('cursadas', $data, array('id' => $data['id']));
    }

    public function delete_cursada($id) {
        $this->db->delete('cursadas', array('id' => $id));
    }
}