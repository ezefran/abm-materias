<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

    public function get_usuario($usuario) {
        $query = $this->db->get_where('usuarios', array('usuario' => $usuario));
        return $query->row_array();
    }

    public function get_usuario_byID($id) {
        $query = $this->db->get_where('usuarios', array('id' => $id));
        return $query->row_array();
    }

    // Cantidades que sirven para la paginacion
    public function get_cantidades($usuario, $items_por_pagina = 8) {
        $select = 'COUNT(usuarios.id) AS cantidad_usuarios, COUNT(usuarios.id)/'.$items_por_pagina.' AS cantidad_paginas';
        $this->db->select($select);
        $this->db->from('usuarios');
        $this->db->join('carreras', 'usuarios.carrera_id = carreras.id');
        $this->db->like('usuarios.apellido', $usuario, 'after');
        $query = $this->db->get();
        $cantidades = $query->row_array();
        $cantidades['cantidad_paginas'] = ceil($cantidades['cantidad_paginas']);
        return $cantidades;
    }

    public function get_cantidad_usuarios() {
        $this->db->select('COUNT(id) AS cantidad_usuarios');
        $this->db->from('usuarios');
        $query = $this->db->get();
        $cantidad_usuarios = $query->row_array()['cantidad_usuarios'];
        return $cantidad_usuarios;
    }

    public function get_usuarios($usuario, $pagina = 1, $items_por_pagina = 8) {
        $select = 'usuarios.id, usuarios.nombre AS u_nombre, usuarios.apellido AS u_apellido,
                usuarios.tipo, usuarios.estado, usuarios.fechaAlta, carreras.nombre AS carrera';
        $this->db->select($select);
        $this->db->from('usuarios');
        $this->db->join('carreras', 'usuarios.carrera_id = carreras.id');
        $this->db->like('usuarios.apellido', $usuario, 'after');
        $this->db->order_by('usuarios.apellido', 'ASC');
        $this->db->order_by('carreras.nombre', 'ASC');
        if ($pagina != 0)
            $this->db->limit($items_por_pagina, ($pagina-1) * $items_por_pagina);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_alumnos() {
        $select = 'id, nombre, apellido, tipo, estado, fechaAlta';
        $this->db->select($select);
        $this->db->from('usuarios');
        $this->db->where('tipo', 1);
        $this->db->where('estado', 1);
        $this->db->order_by('apellido', 'ASC');
        $this->db->order_by('nombre', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    /*
     *    Funciones ABM
     */

    public function insert_usuario($data) {
        $this->db->insert('usuarios', $data);
    }

    public function update_usuario($data) {
        $this->db->update('usuarios', $data, array('id' => $data['id']));
    }

    public function delete_usuario($id) {
        $this->db->delete('usuarios', array('id' => $id));
    }
}