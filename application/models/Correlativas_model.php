<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Correlativas_model extends CI_Model {

    public function get_correlativa($id) {
        $select = 'correlativas.id, M.nombre AS materia, C.nombre AS correlativa, carreras.nombre AS carrera';
        $this->db->select($select);
        $this->db->from('correlativas');
        $this->db->join('materias M', 'correlativas.materia_id = M.id');
        $this->db->join('materias C', 'correlativas.correlativa_id = C.id');
        $this->db->join('carreras', 'carreras.id = C.carrera_id');
        $this->db->where('correlativas.id = '.$id);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Cantidades que sirven para la paginacion
    public function get_cantidades($materia, $items_por_pagina = 8) {
        $select = 'COUNT(correlativas.id) AS cantidad_correlativas, COUNT(correlativas.id)/'.$items_por_pagina.' AS cantidad_paginas';
        $this->db->select($select);
        $this->db->from('correlativas');
        $this->db->join('materias M', 'correlativas.materia_id = M.id');
        $this->db->join('materias C', 'correlativas.correlativa_id = C.id');
        $this->db->join('carreras', 'carreras.id = C.carrera_id');
        $this->db->like('M.nombre', $materia, 'after');
        $query = $this->db->get();
        $cantidades = $query->row_array();
        $cantidades['cantidad_paginas'] = ceil($cantidades['cantidad_paginas']);
        return $cantidades;
    }

    public function get_correlativas($materia, $pagina = 1, $items_por_pagina = 8) {
        $select = 'correlativas.id, M.nombre AS materia, C.nombre AS correlativa, carreras.nombre AS carrera';
        $this->db->select($select);
        $this->db->from('correlativas');
        $this->db->join('materias M', 'correlativas.materia_id = M.id');
        $this->db->join('materias C', 'correlativas.correlativa_id = C.id');
        $this->db->join('carreras', 'carreras.id = C.carrera_id');
        $this->db->like('M.nombre', $materia, 'after');
        $this->db->order_by('M.nombre', 'ASC');
        if ($pagina != 0)
            $this->db->limit($items_por_pagina, ($pagina-1) * $items_por_pagina);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*
     *    Funciones ABM
     */

    public function insert_correlativa($data) {
        $this->db->insert('correlativas', $data);
    }

    public function update_correlativa($data) {
        $this->db->update('correlativas', $data, array('id' => $data['id']));
    }

    public function delete_correlativa($id) {
        $this->db->delete('correlativas', array('id' => $id));
    }

}