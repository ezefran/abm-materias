<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Carreras_model extends CI_Model {

    public function get_carreras() {
        $query = $this->db->get('carreras');
        return $query->result_array();
    }
    
}