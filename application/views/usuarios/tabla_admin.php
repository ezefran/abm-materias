<table class="table is-fullwidth">
	<thead>
		<tr>
			<th>Nombre y apellido</th>
			<th>Carrera</th>
			<th>Tipo</th>
			<th>Estado</th>
			<th>Fecha de alta</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($usuarios as $usuario) { ?>
		<tr id="mat-<?php echo $usuario['id'] ?>">
			<td><?php echo $usuario['u_apellido'].', '.$usuario['u_nombre'] ?></td>
			<td><?php echo $usuario['carrera'] ?></td>
			<td><?php echo $usuario['tipo'] ?></td>
			<td><?php echo $usuario['estado'] ?></td>
			<td><?php echo $usuario['fechaAlta'] ?></td>
			<td>
				<div class="buttons are-small">
					<a class="button is-fullwidth" href="<?php echo base_url().'usuarios/' ?>modificar_usuario?id=<?php echo $usuario['id'] ?>">Modificar</a>
					<a class="button is-danger is-fullwidth" href="#" onclick="borrar(<?php echo $usuario['id'] ?>)">Eliminar</a>
				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<nav class="pagination is-left">
	<a class="button is-primary pagination-next" href="<?php echo base_url().'usuarios/' ?>agregar_usuario">Agregar usuario</a>
	<ul class="pagination-list">
		<?php for ($i = 1; $i <= $cantidad_paginas; $i++) { ?>
		<li id="page-<?php echo $i ?>"><a class="pagination-link <?php echo (($pagina_actual == $i) ? "is-current" : "") ?>" href="#" value="<?php echo $i ?>"><?php echo $i ?></a></li>
		<?php } ?>
	</ul>
</nav>