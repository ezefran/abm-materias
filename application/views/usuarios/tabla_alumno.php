<table class="table is-fullwidth">
	<thead>
		<tr>
			<th>Materia</th>
			<?php if ($titulo != 'En curso') { ?>
			<th>Nota</th>
			<th>Fecha</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($cursadas as $cursada) { ?>
		<tr>
			<td><?php echo $cursada['materia'] ?></td>
			<?php if ($titulo != 'En curso') { ?>
			<td><?php echo $cursada['nota'] ?></td>
			<td><?php echo $cursada['fecha'] ?></td>
			<?php } ?>
		</tr>
		<?php } ?>
	</tbody>
</table>