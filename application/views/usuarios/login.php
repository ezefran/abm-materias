<body>
    <section class="hero">
        <div class="hero-body">
            <div class="container">
            <h1 class="title">ABM de Materias</h1>
            <h2 class="subtitle">Iniciar sesión</h1>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container columns centered">
            <div class="container column is-half">
                <form>
                    <div class="field">
                        <label class="label">Usuario:</label>
                        <div class="control">
                            <input id="usuario" class="input" type="text" maxlength="50">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Contraseña:</label>
                        <div class="control">
                        <input id="contrasena" class="input" type="password" maxlength="50">
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <p class="control">
                            <a id="login" class="button is-primary">Ingresar</a>
                        </p>
                        <p class="control">
                            <a id="registrar_init" class="button is-light">Registrarse</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </section>


    <div id="modal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title"></p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text"></p>
            </section>
            <footer class="modal-card-foot flotar-derecha">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>

    <div id="modal_registro" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Registrar usuario</p>
            </header>
            <section class="modal-card-body">
                <div class="field">
                    <label class="label">Nombre de usuario:</label>
                    <div class="control">
                        <input id="reg_usuario" class="input" type="text" maxlength="50">
                    </div>
                    <p class="help is-danger hidden">Solo se permiten letras</p>
                </div>
                <div class="field">
                    <label class="label">Contraseña:</label>
                    <div class="control">
                        <input id="reg_contrasena" class="input" type="password" maxlength="50">
                    </div>
                    <p class="help is-danger hidden">Solo se permiten letras y números</p>
                </div>
                <div class="field">
                    <label class="label">Nombre:</label>
                    <div class="control">
                        <input id="reg_nombre" class="input" type="text" maxlength="50">
                    </div>
                    <p class="help is-danger hidden">Solo se permiten letras</p>
                </div>
                <div class="field">
                    <label class="label">Apellido:</label>
                    <div class="control">
                        <input id="reg_apellido" class="input" type="text" maxlength="50">
                    </div>
                    <p class="help is-danger hidden">Solo se permiten letras</p>
                </div>
                <div class="field">
                    <label class="label">Carrera:</label>
                    <div class="control">
                        <div class="select">
                            <select id="reg_carrera">
                                <option></option>
                                <?php foreach ($carreras as $carrera) { ?>
                                    <option value=<?php echo $carrera['id'] ?>><?php echo $carrera['nombre'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="modal-card-foot">
                <div class="buttons">
                    <button id="registrar" class="button" data-dismiss="modal" disabled>Registrarse</button>
                    <button id="cerrar_modal_registro" class="button" data-dismiss="modal">Cerrar</button>
                </div>
            </footer>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url().'assets/js/usuarios/login.js' ?>"></script>
</body>