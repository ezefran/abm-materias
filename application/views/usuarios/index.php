<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <div class="control">
                <input id="search" type="text" class="input" placeholder="Buscar...">
            </div>
        </div>
    </form>

    <div id="usuarios" class="table-responsive">
        <?php $this->load->view('usuarios/tabla_admin.php'); ?>
    </div>

    <!-- Modal de Eliminar -->
    <div id="deleteModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Eliminar usuario</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">¿Estás seguro?</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="eliminar_confirmado" class="button is-danger">Eliminar</a>
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/usuarios/' ?>index.js"></script>
</div>
</body>