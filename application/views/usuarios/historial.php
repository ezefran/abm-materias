<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <h2 class="subtitle">Lista de materias <?php echo $descripcion?></h2>
    
    <div id="cursadas">
        <?php $this->load->view('usuarios/tabla_alumno.php'); ?>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/materias/' ?>index.js"></script>
</div>
</body>