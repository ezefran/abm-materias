<body>
<nav class="navbar has-shadow is-spaced">
    <div class="navbar-brand">
        <h1 class="navbar-item"><strong>Panel de Administrador</strong></h1>
        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>
    <div class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item <?php echo ($titulo === 'Inicio'?'is-active':'') ?>" href="<?php echo base_url() ?>admin">Inicio</a>
            <a class="navbar-item <?php echo ($titulo === 'Materias'?'is-active':'') ?>" href="<?php echo base_url() ?>materias">Materias</a>
            <a class="navbar-item <?php echo ($titulo === 'Correlativas'?'is-active':'') ?>" href="<?php echo base_url() ?>correlativas">Correlativas</a>
            <a class="navbar-item <?php echo ($titulo === 'Cursadas'?'is-active':'') ?>" href="<?php echo base_url() ?>cursadas">Cursadas</a>
            <a class="navbar-item <?php echo ($titulo === 'Usuarios'?'is-active':'') ?>" href="<?php echo base_url() ?>usuarios">Usuarios</a>
        </div>
        <div class="navbar-end">
            <div class="navbar-item">
            <a class="button is-primary" id="logout" href="#">Cerrar sesión</a>
            </div>
        </div>
    </div>
</nav>
<script type="text/javascript" src="<?php echo base_url().'assets/js/usuarios/menu.js' ?>"></script>