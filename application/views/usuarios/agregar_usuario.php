<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Nombre de usuario:</label>
            <div class="control">
                <input id="usuario" class="input" type="text" maxlength="50">
            </div>
        </div>
        <div class="field">
            <label class="label">Contraseña:</label>
            <div class="control">
                <input id="contrasena" class="input" type="password" maxlength="50">
            </div>
        </div>
        <div class="field">
            <label class="label">Nombre:</label>
            <div class="control">
                <input id="nombre" class="input" type="text" maxlength="50">
            </div>
        </div>
        <div class="field">
            <label class="label">Apellido:</label>
            <div class="control">
                <input id="apellido" class="input" type="text" maxlength="50">
            </div>
        </div>
        <div class="field">
            <label class="label">Carrera:</label>
            <div class="control">
                <div class="select">
                    <select id="carrera">
                        <option></option>
                        <?php foreach ($carreras as $carrera) { ?>
                            <option value=<?php echo $carrera['id'] ?>><?php echo $carrera['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label">Tipo de usuario:</label>
            <div class="control">
                <div class="select">
                    <select id="tipo">
                        <option></option>
                        <option value=0>Admin</option>
                        <option value=1>Alumno</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="agregar" class="button is-primary" value="Agregar" disabled>
            <a class="button" href="<?php echo base_url().'usuarios/' ?>">Volver</a>
        </div>
    </form>

    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Registro exitoso</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">El usuario fue agregado correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/usuarios/' ?>agregar_usuario.js"></script>
</body>