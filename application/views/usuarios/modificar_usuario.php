<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Nombre de usuario:</label>
            <div class="control">
                <input id="usuario" class="input" type="text" maxlength="50" value="<?php echo $usuario['usuario'] ?>">
            </div>
        </div>
        <div class="field">
            <label class="label">Contraseña:</label>
            <div class="control">
                <input id="contrasena" class="input" type="password" maxlength="50">
            </div>
        </div>
        <div class="field">
            <label class="label">Nombre:</label>
            <div class="control">
                <input id="nombre" class="input" type="text" maxlength="50" value="<?php echo $usuario['nombre'] ?>">
            </div>
        </div>
        <div class="field">
            <label class="label">Apellido:</label>
            <div class="control">
                <input id="apellido" class="input" type="text" maxlength="50" value="<?php echo $usuario['apellido'] ?>">
            </div>
        </div>
        <div class="field">
            <label class="label">Carrera:</label>
            <div class="control">
                <div class="select">
                    <select id="carrera">
                        <option></option>
                        <?php foreach ($carreras as $carrera) { ?>
                        <option value=<?php echo $carrera['id'].' '.($carrera['id'] == $usuario['carrera_id']?'selected':'') ?>><?php echo $carrera['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label">Tipo de usuario:</label>
            <div class="control">
                <div class="select">
                    <select id="tipo">
                        <option></option>
                        <option value=0 <?php echo ($usuario['tipo'] == 0 ? 'selected':'') ?>>Admin</option>
                        <option value=1 <?php echo ($usuario['tipo'] == 1 ? 'selected':'') ?>>Alumno</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label">Estado de usuario:</label>
            <div class="control">
                <div class="select">
                    <select id="estado">
                        <option></option>
                        <option value=1 <?php echo ($usuario['estado'] == 1 ? 'selected':'') ?>>Activo</option>
                        <option value=0 <?php echo ($usuario['estado'] == 0 ? 'selected':'') ?>>Inactivo</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="modificar" class="button is-primary" value="Modificar" disabled>
            <a class="button" href="<?php echo base_url().'usuarios/' ?>">Volver</a>
        </div>
    </form>

    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Registro exitoso</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">El usuario fue modificado correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/usuarios/' ?>modificar_usuario.js"></script>
</body>