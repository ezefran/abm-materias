<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <h2 class="subtitle">Información de la cuenta</h2>
    <form>
        <div class="field">
            <label class="label">Nombre de usuario:</label>
            <div class="control">
                <input value="<?php echo $datos_usuario['usuario'] ?>" class="input" type="text" maxlength="50" disabled>
            </div>
        </div>
        <div class="field">
            <label class="label">Nombre y apellido:</label>
            <div class="control">
                <input value="<?php echo $datos_usuario['nombre'].' '.$datos_usuario['apellido'] ?>" class="input" type="text" maxlength="50" disabled>
            </div>
        </div>
        <div class="field">
            <label class="label">Tipo de cuenta:</label>
            <div class="control">
                <input value="<?php echo ($datos_usuario['tipo'] == 0 ? 'Administrador' : 'Alumno') ?>" class="input" type="text" maxlength="50" disabled>
            </div>
        </div>
        <div class="field">
            <label class="label">Estado de la cuenta:</label>
            <div class="control">
                <input value="<?php echo ($datos_usuario['estado'] == 1 ? 'Activa' : 'Inactiva') ?>" class="input" type="text" maxlength="50" disabled>
            </div>
        </div>
        <div class="field">
            <label class="label">Fecha de alta:</label>
            <div class="control">
                <input value="<?php echo $datos_usuario['fechaAlta'] ?>" class="input" type="text" maxlength="50" disabled>
            </div>
        </div>
    </form>
</div>
</body>