<html>
	<head>
		<title><?php echo $titulo ?></title>
		<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>css/bulma.min.css">
		<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>css/estilos.css">
		<script type="text/javascript" src="<?php echo base_url().'assets/' ?>js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url().'assets/js/utils/' ?>utils.js"></script>
	</head>