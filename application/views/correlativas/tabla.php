<table class="table is-fullwidth">
	<thead>
		<tr>
			<th>Materia</th>
			<th>Correlativa</th>
			<th>Carrera</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($correlativas as $correlativa) { ?>
		<tr id="mat-<?php echo $correlativa['id'] ?>">
			<td><?php echo $correlativa['materia'] ?></td>
			<td><?php echo $correlativa['correlativa'] ?></td>
			<td><?php echo $correlativa['carrera'] ?></td>
			<td>
				<div class="buttons are-small">
					<a class="button is-fullwidth" href="<?php echo base_url().'correlativas/' ?>modificar_correlativa?id=<?php echo $correlativa['id'] ?>">Modificar</a>
					<a class="button is-danger is-fullwidth" href="#" onclick="borrar(<?php echo $correlativa['id'] ?>)">Eliminar</a>
				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<nav class="pagination is-left">
	<a class="button is-primary pagination-next" href="<?php echo base_url().'correlativas/' ?>agregar_correlativa">Agregar correlativa</a>
	<ul class="pagination-list">
		<?php for ($i = 1; $i <= $cantidad_paginas; $i++) { ?>
		<li id="page-<?php echo $i ?>"><a class="pagination-link <?php echo (($pagina_actual == $i) ? "is-current" : "") ?>" href="#" value="<?php echo $i ?>"><?php echo $i ?></a></li>
		<?php } ?>
	</ul>
</nav>