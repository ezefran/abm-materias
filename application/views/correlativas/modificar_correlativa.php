<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Materia:</label>
            <div class="control">
                <div class="select">
                <select id="materia">
                    <option></option>
                    <?php foreach ($materias as $materia) { ?>
                            <option value=<?php echo $materia['id'].' '.($correlativa['materia'] == $materia['nombre']?'selected':'') ?>><?php echo $materia['nombre'] ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label">Correlativa:</label>
            <div class="control">
                <div class="select">
                <select id="correlativa">
                    <option></option>
                    <?php foreach ($materias as $materia) { ?>
                        <option value=<?php echo $materia['id'].' '.($correlativa['correlativa'] == $materia['nombre']?'selected':'') ?>><?php echo $materia['nombre'] ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="modificar" class="button is-primary" value="Modificar" disabled>
            <a class="button" href="<?php echo base_url().'correlativas/' ?>">Volver</a>
        </div>
    </form>
    
    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Modificación exitoso</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">La correlatividad fue modificada correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/correlativas/' ?>modificar_correlativa.js"></script>
</body>