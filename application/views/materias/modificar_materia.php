<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Nombre:</label>
            <div class="control">
                <input id="nombre" class="input" type="text" maxlength="50" value="<?php echo $materia['nombre'] ?>">
            </div>
        </div>
        <div class="field">
            <label class="label">Carrera:</label>
            <div class="control">
                <div class="select">
                <select id="carrera">
                    <option></option>
                    <?php foreach ($carreras as $carrera) { ?>
                        <option value=<?php echo $carrera['id'].' '.($carrera['nombre'] == $materia['carrera']?'selected':'') ?>><?php echo $carrera['nombre'] ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label">Descripción:</label>
            <div class="control">
                <textarea id="descripcion" class="textarea" rows="5" maxlength="255"><?php echo $materia['descripcion'] ?></textarea>
            </div>
        </div>
        <div class="field">
            <label class="label">Carga horaria:</label>
            <!-- Lista desplegable que muestra las opciones 2, 4, 6, 8 y 10 -->
            <div class="control">
                <div class="select">
                    <select id="carga_horaria">
                        <option></option>
                        <?php for($i = 2; $i <= 10; $i = $i + 2) { ?>
                        <option <?php echo ($materia['carga_horaria'] == $i?'selected':'') ?>><?php echo $i ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="modificar" class="button is-primary" value="Modificar" disabled>
            <a class="button" href="<?php echo base_url().'materias/' ?>">Volver</a>
        </div>
    </form>

    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Modificación exitosa</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">La materia fue modificada correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/materias/' ?>modificar_materia.js"></script>
</body>