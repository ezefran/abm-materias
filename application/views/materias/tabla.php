<table class="table is-fullwidth">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Descripción</th>
			<th>Carga horaria</th>
			<th>Carrera</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($materias as $materia) { ?>
		<tr id="mat-<?php echo $materia['id'] ?>">
			<td><?php echo $materia['nombre'] ?></td>
			<td><?php echo $materia['descripcion'] ?></td>
			<td><?php echo $materia['carga_horaria'] ?> horas</td>
			<td><?php echo $materia['carrera'] ?></td>
			<td>
				<div class="buttons are-small">
					<a class="button is-fullwidth" href="<?php echo base_url().'materias/' ?>modificar_materia?id=<?php echo $materia['id'] ?>">Modificar</a>
					<a class="button is-danger is-fullwidth" href="#" onclick="borrar(<?php echo $materia['id'] ?>)">Eliminar</a>
				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<nav class="pagination is-left">
	<a class="button is-primary pagination-next" href="<?php echo base_url().'materias/' ?>agregar_materia">Agregar materia</a>
	<ul class="pagination-list">
		<?php for ($i = 1; $i <= $cantidad_paginas; $i++) { ?>
		<li id="page-<?php echo $i ?>"><a class="pagination-link <?php echo (($pagina_actual == $i) ? "is-current" : "") ?>" href="#" value="<?php echo $i ?>"><?php echo $i ?></a></li>
		<?php } ?>
	</ul>
</nav>