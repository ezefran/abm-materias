<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Alumno:</label>
            <div class="control">
                <div class="select">
                    <select id="usuario">
                        <option value=""></option>
                        <?php foreach ($usuarios as $usuario) { ?>
                            <option value=<?php echo $usuario['id'] ?>><?php echo $usuario['nombre'].' '.$usuario['apellido'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div id="div-materia" class="field" hidden>
            <label class="label">Materia:</label>
            <div class="control">
                <div class="select">
                    <select id="materia">
                        <?php $this->load->view('cursadas/materias'); ?>
                    </select>
                </div>
            </div>
        </div>
        <div id="hay_materia" hidden>
            <div class="field">
                <label class="label">Estado:</label>
                <div class="control">
                    <div class="select">
                        <select id="estado">
                            <option value=""></option>
                            <option value="EN CURSO">En curso</option>
                            <option value="APROBADA">Aprobada</option>
                            <option value="FINAL">Final</option>
                            <option value="RECURSA">Recursa</option>
                            <option value="ABANDONO">Abandono</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="hay_estado" hidden>
                <div class="field">
                    <label class="label">Nota:</label>
                    <div class="control">
                        <div class="select">
                            <select id="nota">
                                <option value=""></option>
                                <?php for($i = 1; $i <= 10; $i++) { ?>
                                    <option><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Fecha:</label>
                    <div class="control">
                        <input id="fecha" class="input" type="date">
                    </div>
                </div>
            </div>
        </div>
        <div id="hay_materia" hidden>
            <div class="form-group">
                <label>Estado:
                    <select id="estado" class="form-control">
                        <option value=""></option>
                        <option value="EN CURSO">En curso</option>
                        <option value="APROBADA">Aprobada</option>
                        <option value="FINAL">Final</option>
                        <option value="RECURSA">Recursa</option>
                        <option value="ABANDONO">Abandono</option>
                    </select>
                </label>
            </div>
            <div id="hay_estado" hidden>
                <div class="form-group">
                    <label>Nota:
                        <select id="nota" class="form-control">
                            <option value=""></option>
                            <?php for($i = 1; $i <= 10; $i++) { ?>
                                <option><?php echo $i ?></option>
                            <?php } ?>
                        </select>
                    </label>
                </div>
                <div class="form-group">
                    <label>Fecha:
                        <input id="fecha" class="form-control" type="date">
                    </label>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="agregar" class="button is-primary" value="Agregar" disabled>
            <a class="button" href="<?php echo base_url().'cursadas/' ?>">Volver</a>
        </div>
    </form>

    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Registro exitoso</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">La cursada fue agregada correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/cursadas/' ?>agregar_cursada.js"></script>
</body>