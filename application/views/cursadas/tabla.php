<table class="table is-fullwidth">
	<thead>
		<tr>
			<th>Materia</th>
			<th>Alumno</th>
			<th>Estado</th>
			<th>Nota</th>
			<th>Fecha</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($cursadas as $cursada) { ?>
		<tr id="mat-<?php echo $cursada['id'] ?>">
			<td><?php echo $cursada['m_nombre'] ?></td>
			<td><?php echo $cursada['u_nombre'].' '.$cursada['u_apellido'] ?></td>
			<td><?php echo $cursada['estado'] ?></td>
			<td><?php echo $cursada['nota'] ?></td>
			<td><?php echo $cursada['fecha'] ?></td>
			<td>
				<div class="buttons are-small">
					<a class="button is-fullwidth" href="<?php echo base_url().'cursadas/' ?>modificar_cursada?id=<?php echo $cursada['id'] ?>">Modificar</a>
					<a class="button is-danger is-fullwidth" href="#" onclick="borrar(<?php echo $cursada['id'] ?>)">Eliminar</a>
				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<div class="pagination-centered">
	<nav class="pagination is-left">
		<a class="button is-primary pagination-next" href="<?php echo base_url().'cursadas/' ?>agregar_cursada">Agregar cursada</a>
		<ul class="pagination-list">
			<?php for ($i = 1; $i <= $cantidad_paginas; $i++) { ?>
			<li id="page-<?php echo $i ?>"><a class="pagination-link <?php echo (($pagina_actual == $i) ? "is-current" : "") ?>" href="#" value="<?php echo $i ?>"><?php echo $i ?></a></li>
			<?php } ?>
		</ul>
	</nav>
</div>