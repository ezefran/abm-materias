<div class="container centrado">
    <h1 class="title"><?php echo $titulo ?></h1>
    <form>
        <div class="field">
            <label class="label">Alumno:</label>
            <div class="control">
                <input id="usuario" data-usuario="<?php echo $cursada['u_id'] ?>" class="input" type="text" value="<?php echo $cursada['u_nombre'].' '.$cursada['u_apellido'] ?>" readonly>
            </div>
        </div>
        <div class="field">
            <label class="label">Materia:</label>
            <div class="control">
                <input id="materia" data-materia="<?php echo $cursada['m_id'] ?>" class="input" type="text" value="<?php echo $cursada['m_nombre'] ?>" readonly>
            </div>
        </div>
        <div class="field">
            <label class="label">Estado:</label>
            <div class="control">
                <div class="select">
                    <select id="estado">
                        <option value="EN CURSO" <?php echo ('EN CURSO' == $cursada['estado'] ? 'selected':'') ?>>En curso</option>
                        <option value="APROBADA" <?php echo ('APROBADA' == $cursada['estado'] ? 'selected':'') ?>>Aprobada</option>
                        <option value="FINAL" <?php echo ('FINAL' == $cursada['estado'] ? 'selected':'') ?>>Final</option>
                        <option value="RECURSA" <?php echo ('RECURSA' == $cursada['estado'] ? 'selected':'') ?>>Recursa</option>
                        <option value="ABANDONO" <?php echo ('ABANDONO' == $cursada['estado'] ? 'selected':'') ?>>Abandono</option>
                    </select>
                </div>
            </div>
        </div>
        <div id="hay_estado" <?php echo ($cursada['nota'] ?: 'hidden') ?>>
            <div class="field">
                <label class="label">Nota:</label>
                <div class="control">
                    <div class="select">
                        <select id="nota">
                            <option value=""></option>
                            <?php for($i = 1; $i <= 10; $i++) { ?>
                                <option <?php echo ($cursada['nota'] == $i?'selected':'') ?>><?php echo $i ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field">
                <label class="label">Fecha:</label>
                <div class="control">
                <input id="fecha" class="input" type="date" <?php echo 'value="'.(in_array($cursada['estado'], array('EN CURSO', 'ABANDONO')) ? '' : $cursada['fecha']).'"' ?>>
                </div>
            </div>
        </div>
        <div class="buttons flotar-derecha">
            <input type="button" id="modificar" class="button is-primary" value="Modificar">
            <a class="button" href="<?php echo base_url().'cursadas/' ?>">Volver</a>
        </div>
    </form>

    <!-- Modal de Exito -->
    <div id="successModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title modal-title">Modificación exitosa</p>
            </header>
            <section class="modal-card-body">
                <p id="modal_text">La cursada fue modificada correctamente.</p>
            </section>
            <footer class="modal-card-foot" style="justify-content: flex-end;">
                <a id="cerrar_modal" class="button">Cerrar</a>
            </footer>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/cursadas/' ?>modificar_cursada.js"></script>
</body>