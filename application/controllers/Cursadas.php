<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cursadas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cursadas_model');
        $this->load->model('materias_model');
        $this->load->model('usuarios_model');
        $this->load->library('permisos');
    }

    /*
     *    Funcion de la pantalla principal de cursadas.
     *    Se encarga de filtrar las cursadas en caso de que
     *    se haya realizado una busqueda y también se encarga
     *    de eliminar las cursadas cuando el usuario lo solicita.
     */

    public function index() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id'])) {
                // Eliminar cursada
                $this->cursadas_model->delete_cursada($_POST['id']);
            }
            else {
                $busqueda = $this->input->get('q');
                $pagina = $this->input->get('p');
                // Mostrar cursadas
                if (isset($busqueda) || isset($pagina)) {
                    $data['pagina_actual'] = ($pagina ? $pagina : 1);
                    $cantidades = $this->cursadas_model->get_cantidades($busqueda);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['cursadas'] = $this->cursadas_model->get_cursadas($busqueda, $pagina);
                    $this->load->view('cursadas/tabla', $data);
                }
                else {
                    $data['pagina_actual'] = 1;
                    $cantidades = $this->cursadas_model->get_cantidades(null);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['titulo'] = 'Cursadas';
                    $this->load->view('templates/header', $data);
                    $data['cursadas'] = $this->cursadas_model->get_cursadas(null);
                    $this->load->view('usuarios/navbar_admin', $data);
                    $this->load->view('cursadas/index', $data);
                    $this->load->view('templates/footer', $data);
                }
            }
        }
        else
            redirect('login');
    }

    /*
     *    Funciones para agregar y modificar cursadas de la
     *    base de datos.
     */

    public function agregar_cursada() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['usuario_id']) && isset($_POST['materia_id']) &&
            isset($_POST['nota']) && isset($_POST['fecha'])) {
                $this->cursadas_model->insert_cursada($_POST);
            }
            else if (isset($_POST['usuario_id'])) {
                // Carga las materias cursables
                if ($_POST['usuario_id']) {
                    $data['materias'] = $this->materias_model->get_materias_cursables($_POST['usuario_id']);
                    $this->load->view('cursadas/materias', $data);
                }
                else
                    echo 'NO_USUARIO';
            }
            else {
                $data['titulo'] = 'Cursadas - Agregar cursada';
                $data['materias'] = $this->materias_model->get_materias(null);
                $data['usuarios'] = $this->usuarios_model->get_alumnos();
                $this->load->view('templates/header', $data);
                $this->load->view('cursadas/agregar_cursada');
                $this->load->view('templates/footer', $data);
            }
        }
        else
            redirect('login');
    }

    public function modificar_cursada() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id']) && isset($_POST['usuario_id']) && isset($_POST['materia_id']) &&
            isset($_POST['nota']) && isset($_POST['fecha'])) {
                $this->cursadas_model->update_cursada($_POST);
            }
            else {
                $data['titulo'] = 'Cursadas - Modificar cursada';
                $data['cursada'] = $this->cursadas_model->get_cursada($_GET['id']);
                $data['materias'] = $this->materias_model->get_materias(null);
                $data['usuarios'] = $this->usuarios_model->get_alumnos();
                $this->load->view('templates/header', $data);
                $this->load->view('cursadas/modificar_cursada', $data);
                $this->load->view('templates/footer', $data);
            }
        }
        else
            redirect('login');
    }

}