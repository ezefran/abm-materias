<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Materias extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('materias_model');
        $this->load->model('carreras_model');
        $this->load->library('permisos');
    }

    /*
     *    Funcion de la pantalla principal de materias.
     *    Se encarga de filtrar las materias en caso de que
     *    se haya realizado una busqueda y también se encarga
     *    de eliminar las materias cuando el usuario lo solicita.
     */

    public function index() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id'])) {
                // Eliminar materia
                $this->materias_model->delete_materia($_POST['id']);
            }
            else {
                $busqueda = $this->input->get('q');
                $pagina = $this->input->get('p');
                // Mostrar materias
                if (isset($busqueda) || isset($pagina)) {
                    $data['pagina_actual'] = ($pagina ? $pagina : 1);
                    $cantidades = $this->materias_model->get_cantidades($busqueda);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['materias'] = $this->materias_model->get_materias($busqueda, $pagina);
                    $this->load->view('materias/tabla', $data);
                }
                else {
                    $data['pagina_actual'] = 1;
                    $cantidades = $this->materias_model->get_cantidades(null);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['titulo'] = 'Materias';
                    $this->load->view('templates/header', $data);
                    $data['materias'] = $this->materias_model->get_materias(null);
                    $this->load->view('usuarios/navbar_admin', $data);
                    $this->load->view('materias/index', $data);
                    $this->load->view('templates/footer', $data);
                }
            }
        }
        else
            redirect('login');
    }

    /*
     *    Funciones para agregar y modificar materias de la
     *    base de datos.
     */

    public function agregar_materia() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['nombre']) && isset($_POST['descripcion']) &&
            isset($_POST['carga_horaria']) && isset($_POST['carrera_id'])) {
                $this->materias_model->insert_materia($_POST);
            }
            else {
                $data['titulo'] = 'Materias - Agregar materia';
                $data['carreras'] = $this->carreras_model->get_carreras();
                $this->load->view('templates/header', $data);
                $this->load->view('usuarios/navbar_admin', $data);
                $this->load->view('materias/agregar_materia', $data);
                $this->load->view('templates/footer', $data);
            }
        }
        else
            redirect('login');
    }

    public function modificar_materia() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['descripcion']) &&
            isset($_POST['carga_horaria']) && isset($_POST['carrera_id'])) {
                $this->materias_model->update_materia($_POST);
            }
            $data['titulo'] = 'Materias - Modificar materia';
            $data['materia'] = $this->materias_model->get_materia($this->input->get('id'));
            $data['carreras'] = $this->carreras_model->get_carreras();
            $this->load->view('templates/header', $data);
            $this->load->view('usuarios/navbar_admin', $data);
            $this->load->view('materias/modificar_materia', $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }
}