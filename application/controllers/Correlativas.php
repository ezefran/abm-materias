<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Correlativas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('correlativas_model');
        $this->load->model('materias_model');
        $this->load->library('permisos');
    }

    /*
     *    Funcion de la pantalla principal de correlativas.
     *    Se encarga de filtrar las correlativas en caso de que
     *    se haya realizado una busqueda y también se encarga
     *    de eliminar las correlativas cuando el usuario lo solicita.
     */

    public function index() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id'])) {
                // Eliminar correlativa
                $this->correlativas_model->delete_correlativa($_POST['id']);
            }
            else {
                $busqueda = $this->input->get('q');
                $pagina = $this->input->get('p');
                // Mostrar correlativas
                if (isset($busqueda) || isset($pagina)) {
                    $data['pagina_actual'] = ($pagina ? $pagina : 1);
                    $cantidades = $this->correlativas_model->get_cantidades($busqueda);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['correlativas'] = $this->correlativas_model->get_correlativas($busqueda, $pagina);
                    $this->load->view('correlativas/tabla', $data);
                }
                else {
                    $data['pagina_actual'] = 1;
                    $cantidades = $this->correlativas_model->get_cantidades(null);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['titulo'] = 'Correlativas';
                    $this->load->view('templates/header', $data);
                    $data['correlativas'] = $this->correlativas_model->get_correlativas(null);
                    $this->load->view('usuarios/navbar_admin', $data);
                    $this->load->view('correlativas/index', $data);
                    $this->load->view('templates/footer', $data);
                }
            }
        }
        else
            redirect('login');
    }

    /*
     *    Funciones para agregar y modificar correlativas de la
     *    base de datos.
     */

    public function agregar_correlativa() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['materia_id']) && isset($_POST['correlativa_id'])) {
                $this->correlativas_model->insert_correlativa($_POST);
            }
            else {
                $data['titulo'] = 'Correlativas - Agregar correlativa';
                $data['materias'] = $this->materias_model->get_materias(null);
                $this->load->view('templates/header', $data);
                $this->load->view('usuarios/navbar_admin', $data);
                $this->load->view('correlativas/agregar_correlativa', $data);
                $this->load->view('templates/footer', $data);
            }
        }
        else
            redirect('login');
    }

    public function modificar_correlativa() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id']) && isset($_POST['materia_id']) && isset($_POST['correlativa_id'])) {
                $this->correlativas_model->update_correlativa($_POST);
            }
            $data['titulo'] = 'Correlativas - Modificar correlativa';
            $data['correlativa'] = $this->correlativas_model->get_correlativa($this->input->get('id'));
            $data['materias'] = $this->materias_model->get_materias(null, 0);
            $this->load->view('templates/header', $data);
            $this->load->view('usuarios/navbar_admin', $data);
            $this->load->view('correlativas/modificar_correlativa', $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

}