<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('usuarios_model');
        $this->load->model('cursadas_model');
        $this->load->model('materias_model');
        $this->load->model('carreras_model');
        $this->load->library('permisos');
    }

    /*
     *    Funcion de la pantalla principal de usuarios.
     *    Se encarga de filtrar los usuarios en caso de que
     *    se haya realizado una busqueda y también se encarga
     *    de eliminar los usuarios cuando el usuario lo solicita.
     */

    public function index() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id'])) {
                // Eliminar usuario
                $this->usuarios_model->delete_usuario($_POST['id']);
            }
            else {
                $busqueda = $this->input->get('q');
                $pagina = $this->input->get('p');
                // Mostrar usuarios
                if (isset($busqueda) || isset($pagina)) {
                    $data['pagina_actual'] = ($pagina ? $pagina : 1);
                    $cantidades = $this->usuarios_model->get_cantidades($busqueda);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['usuarios'] = $this->usuarios_model->get_usuarios($busqueda, $pagina);
                    $this->load->view('usuarios/tabla_admin', $data);
                }
                else {
                    $data['pagina_actual'] = 1;
                    $cantidades = $this->usuarios_model->get_cantidades(null);
                    $data['cantidad_paginas'] = $cantidades['cantidad_paginas'];
                    $data['titulo'] = 'Usuarios';
                    $this->load->view('templates/header', $data);
                    $data['usuarios'] = $this->usuarios_model->get_usuarios(null);
                    $this->load->view('usuarios/navbar_admin', $data);
                    $this->load->view('usuarios/index', $data);
                    $this->load->view('templates/footer', $data);
                }
            }
        }
        else
            redirect('login');
    }

    /*
     *    Funciones para agregar y modificar usuarios de la
     *    base de datos.
     */

    public function agregar_usuario() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['usuario']) && isset($_POST['hash'])
            && isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['carrera_id'])
            && isset($_POST['tipo']) && isset($_POST['estado'])) {
                $data = $_POST;
                $data['hash'] = password_hash($_POST['hash'], PASSWORD_BCRYPT);
                $this->usuarios_model->insert_usuario($data);
            }
            else {
                $data['titulo'] = 'Usuarios - Agregar usuario';
                $data['carreras'] = $this->carreras_model->get_carreras();
                $this->load->view('templates/header', $data);
                $this->load->view('usuarios/agregar_usuario', $data);
                $this->load->view('templates/footer', $data);
            }
        }
        else
            redirect('login');
    }

    public function modificar_usuario() {
        if ($this->permisos->isAdmin()) {
            if (isset($_POST['id']) && isset($_POST['usuario']) && isset($_POST['hash'])
            && isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['carrera_id'])
            && isset($_POST['tipo']) && isset($_POST['estado'])) {
                $data = $_POST;
                $data['hash'] = password_hash($_POST['hash'], PASSWORD_BCRYPT);
                $this->usuarios_model->update_usuario($data);
            }
            $data['titulo'] = 'Usuarios - Modificar usuario';
            $data['usuario'] = $this->usuarios_model->get_usuario_byID($this->input->get('id'));
            $data['carreras'] = $this->carreras_model->get_carreras();
            $this->load->view('templates/header', $data);
            $this->load->view('usuarios/modificar_usuario', $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    /*
     *    Funciones que llevan a cabo el ingreso de sesion
     *    de los usuarios.
     */
    
    public function login() {
        if (isset($this->session->datos_usuario)) {
            $datos_usuario = $this->session->datos_usuario;
            if ($datos_usuario['tipo'] == 0) {
                redirect('admin');
                return;
            }
            else if ($datos_usuario['tipo'] == 1) {
                redirect('alumno');
                return;
            }
        }
        $data['titulo'] = 'Iniciar sesión';
        $data['carreras'] = $this->carreras_model->get_carreras();
        $this->load->view("templates/header", $data);
        $this->load->view("usuarios/login");
        $this->load->view('templates/footer', $data);
    }

    public function logout() {
        $this->session->unset_userdata('datos_usuario');
        redirect('login');
    }

    public function validar_login() {
        if (isset($_POST['usuario']) && isset($_POST['contrasena'])) {
            $usuario = $_POST['usuario'];
            $contrasena = $_POST['contrasena'];

            $datos_usuario = $this->usuarios_model->get_usuario($usuario);
            if (empty($datos_usuario)) {
                echo "ERROR_USUARIO_NO_EXISTE";
                return;
            }
            if (password_verify($contrasena, $datos_usuario['hash'])) {
                if ($datos_usuario['estado'] == 0) {
                    echo "ERROR_USUARIO_INACTIVO";
                    return;
                }
                $this->session->datos_usuario = $datos_usuario;
            }
            else {
                echo "ERROR_CONTRASENA_INCORRECTA";
                return;
            }
        }
    }

    /*
     *    Funcion de registro de usuario (alumno)
     */

     public function registrar_alumno() {
        if (isset($_POST['usuario']) && isset($_POST['hash'])
        && isset($_POST['nombre']) && isset($_POST['apellido'])
        && isset($_POST['carrera_id'])) {
            $data = $_POST;
            $data['hash'] = password_hash($_POST['hash'], PASSWORD_BCRYPT);
            $data['tipo'] = 1;
            $data['estado'] = 1;
            $this->usuarios_model->insert_usuario($data);
        }
     }


    /*
     *    Funciones que, luego de haber ingresado al sistema,
     *    cargan la vista de inicio dependiendo del tipo de usuario.
     */

    public function admin() {
        if ($this->permisos->isAdmin()) {
            $datos_usuario = $this->session->datos_usuario;
            $data['titulo'] = 'Inicio';
            $data['datos_usuario'] = $datos_usuario;
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/navbar_admin", $data);
            $this->load->view("usuarios/cuenta", $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    public function alumno() {
        if ($this->permisos->isAlumno()) {
            $datos_usuario = $this->session->datos_usuario;
            $data['titulo'] = 'Inicio';
            $data['datos_usuario'] = $datos_usuario;
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/navbar_alumno", $data);
            $this->load->view("usuarios/cuenta", $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    /*
     *    Funciones que cargan la vista con las materias cursadas por el alumno
     *    dependiendo de si estan aprobadas, en final o en curso.
     */

    public function historial() {
        if ($this->permisos->isAlumno()) {
            $datos_usuario = $this->session->datos_usuario;
            $data['titulo'] = 'Historial';
            $data['datos_usuario'] = $datos_usuario;
            $data['cursadas'] = $this->cursadas_model->get_historial($datos_usuario['id'], 'APROBADA');
            $data['descripcion'] = "aprobadas";
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/navbar_alumno", $data);
            $this->load->view("usuarios/historial", $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    public function finales() {
        if ($this->permisos->isAlumno()) {
            $datos_usuario = $this->session->datos_usuario;
            $data['titulo'] = 'Finales';
            $data['datos_usuario'] = $datos_usuario;
            $data['cursadas'] = $this->cursadas_model->get_historial($datos_usuario['id'], 'FINAL');
            $data['descripcion'] = "pendientes de final";
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/navbar_alumno", $data);
            $this->load->view("usuarios/historial", $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    public function en_curso() {
        if ($this->permisos->isAlumno()) {
            $datos_usuario = $this->session->datos_usuario;
            $data['titulo'] = 'En curso';
            $data['datos_usuario'] = $datos_usuario;
            $data['cursadas'] = $this->cursadas_model->get_historial($datos_usuario['id'], 'EN CURSO');
            $data['descripcion'] = "en curso";
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/navbar_alumno", $data);
            $this->load->view("usuarios/historial", $data);
            $this->load->view('templates/footer', $data);
        }
        else
            redirect('login');
    }

    /*
     *    Funcion que carga la lista de materias que el alumno
     *    esta habilitado para cursar.
     */

    public function cursables() {
        $usuario_id = $this->input->get('id');
        if($usuario_id) {
            $data['materias'] = $this->materias_model->get_materias_cursables($usuario_id);
            $this->load->view('cursadas/materias', $data);
        }
    }

    /*
     *    Funcion que carga la informacion del usuario
     */

    public function cuenta() {
        if ($this->session->has_userdata('datos_usuario')) {
            $data['titulo'] = 'Iniciar sesión';
            $data['datos_usuario'] = $this->session->datos_usuario;
            $this->load->view("templates/header", $data);
            $this->load->view("usuarios/cuenta", $data);
            $this->load->view('templates/footer', $data);
        }
    }

}