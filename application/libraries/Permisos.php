<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permisos {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }

    // Verifica que la sesion actual pertenece a un administrador
    public function isAdmin() {
        if ($this->CI->session->has_userdata('datos_usuario') && $this->CI->session->userdata('datos_usuario')['tipo'] == 0)
            return true;
        return false;
    }

    // Verifica que la sesion actual pertenece a un alumno
    public function isAlumno() {
        if ($this->CI->session->has_userdata('datos_usuario') && $this->CI->session->userdata('datos_usuario')['tipo'] == 1)
            return true;
        return false;
    }
}