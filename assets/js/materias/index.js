var pagina, busqueda;

seleccionarPagina = function() {
	$(".pagination-link").click(function() {
		var pagina = $(this).text();
		Utils.seleccionarPagina("materias", pagina, busqueda);
	});
}

$(document).ready(function() {
	// Se ejecuta cuando se escribe en el input de busqueda
	$("#search").keyup(function() {
		busqueda = $(this).val()
		Utils.filtrarLista("materias", pagina, busqueda);
	})
	
	// Se ejecuta cuando se hace click en el boton Eliminar en el modal
	$("#eliminar_confirmado").click(function() {
		Utils.eliminarElemento("materias", $(this).val())
	});

	// Se ejecuta cuando se hace click en uno de los botones de paginacion
	seleccionarPagina();

	$("#deleteModal").click(function() {
		$(this).toggleClass("is-active");
	});

	/* Se ejecuta despues de cada request Ajax.
	Permite seleccionar elementos del nuevo html generado. */
	$(document).ajaxStop(function() {
		seleccionarPagina();
	});
});

// Se ejecuta cuando se hace click sobre el boton Eliminar que esta al lado de una materia
function borrar(id) {
	// Modal para confirmar la eliminacion
	$(".modal-body").html('<h4>¿Estás seguro de que querés eliminar la materia ' + $("#mat-" + id + " > td:first").html() + '?</h4>');
	$("#eliminar_confirmado").val(id);
	$("#deleteModal").toggleClass("is-active");
};