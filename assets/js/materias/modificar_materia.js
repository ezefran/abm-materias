// Regex para validar parametros
var regexDesc = new RegExp('[^A-Za-zñ,;. ]');
var regexNom = new RegExp('[^A-Za-zñ ]');

// Valida que los inputs no esten vacios
var validar_inputs = function() {
	if ($("#nombre").val().trim() != '' && $("#carrera").find(":selected").val() != ''
	&& $("#carga_horaria").find(":selected").val() != '' && !regexNom.test($("#nombre").val().trim())
	&& !regexDesc.test($("#descripcion").val().trim()))
		$("#modificar").prop('disabled', false);
	else
		$("#modificar").prop('disabled', true);
}

$(document).ready(function() {
	$("#modificar").click(function() {		
		// Metodo POST con Ajax para actualizar la materia con los datos ingresados
		$.post("/materias/modificar_materia", {
			id: $(location).attr('search').split('=')[1],
			nombre: $("#nombre").val().trim(),
			descripcion: $("#descripcion").val().trim(),
			carrera_id: $("#carrera").find(":selected").val(),
			carga_horaria: $("#carga_horaria").find(":selected").val()
		}, function() {
			// Se carga el modal de exito
			$("#successModal").toggleClass("is-active");
			$("#successModal").toggleClass("is-clipped");
		});
		
	});
	
	// Validaciones de nombre y descripcion en tiempo real (marco rojo):
	$("#nombre").keyup(function() {
		var nombre = $("#nombre").val().trim();
		if(regexNom.test(nombre)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});
	
	$("#descripcion").keyup(function() {
		var descripcion = $(this).val().trim();
		if(regexDesc.test(descripcion)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});

	// Cuando cambia cualquier input (menos descripcion) se valida que los campos no esten vacios
	$("#nombre").change(validar_inputs);
	$("#carrera").change(validar_inputs);
	$("#carga_horaria").change(validar_inputs);
	  
	$("#cerrar_modal").click(function() {
		$("#successModal").toggleClass("is-active");
		$("#successModal").toggleClass("is-clipped");
	});
	
});