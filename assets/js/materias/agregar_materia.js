// Regex para validar parametros
var regexDesc = new RegExp('[^A-Za-zñ,;. ]');
var regexNom = new RegExp('[^A-Za-zñ ]');

// Valida que los inputs no esten vacios
var validar_inputs = function() {
	if ($("#nombre").val().trim() != '' && $("#carrera").find(":selected").val() != ''
	&& $("#carga_horaria").find(":selected").val() != '' && !regexNom.test($("#nombre").val().trim())
	&& !regexDesc.test($("#descripcion").val().trim()))
		$("#agregar").prop('disabled', false);
	else
		$("#agregar").prop('disabled', true);
}

$(document).ready(function() {
	$("#agregar").click(function() {
		// Metodo POST con Ajax para cargar la materia con los datos ingresados
		$.post("/materias/agregar_materia", {
			nombre: $("#nombre").val().trim(),
			descripcion: $("#descripcion").val().trim(),
			carrera_id: $("#carrera").find(":selected").val(),
			carga_horaria: $("#carga_horaria").find(":selected").val()
		}, function() {
			// Se carga el modal de exito
			$("#successModal").toggleClass("is-active");
			$("#successModal").toggleClass("is-clipped");
		});
		
	});
	
	// Validaciones de nombre y descripcion en tiempo real (marco rojo):
	$("#nombre").keyup(function() {
		var nombre = $("#nombre").val().trim();
		if(regexNom.test(nombre)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});
	
	$("#descripcion").keyup(function() {
		var descripcion = $(this).val().trim();
		if(regexDesc.test(descripcion)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});

	// Cuando cambia cualquier input (menos descripcion) se valida que los campos no esten vacios
	$("#nombre").change(validar_inputs);
	$("#carrera").change(validar_inputs);
	$("#carga_horaria").change(validar_inputs);

	$("#cerrar_modal").click(function() {
        $("#successModal").toggleClass("is-active");
        $("#successModal").toggleClass("is-clipped");
	});
	
});