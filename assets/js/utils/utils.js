var Utils = {};
var regexBus = new RegExp('[^A-Za-z0-9ñ$@ ]');

Utils.seleccionarPagina = function(nombre_pagina, pagina, busqueda) {
	$.get("?p=" + pagina + (busqueda ? "&q=" + busqueda : ""), function(data, status) {
		$("#" + nombre_pagina).html(data);
	});
};

Utils.filtrarLista = function(nombre_pagina, pagina, busqueda) {
	// Validacion del input
	if (regexBus.test(busqueda)) {
		$("#search").parent().addClass('has-error');
		return;
	} else {
		$("#search").parent().removeClass('has-error');
	}
	// Metodo GET con Ajax para filtrar la lista de materias
	$.get((pagina ? "?p=" + pagina + "&" : "?") + "q=" + busqueda, function(data, status) {
		$("#" + nombre_pagina).html(data);
	});	
}

Utils.eliminarElemento = function(nombre_pagina, id) {
	$("#mat-" + id).fadeOut(300);
	// Metodo POST con Ajax para eliminar el elemento seleccionado
	$.post("/" + nombre_pagina, {
		id: id
	}, function() {
		// Recarga la pagina para mostrar el nuevo orden
		window.location.reload();
	});
}