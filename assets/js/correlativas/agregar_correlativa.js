// Valida que los inputs no esten vacios
var validar_inputs = function() {
	if($("#materia").find(":selected").val() != '' && $("#correlativa").find(":selected").val() != '')
		$("#agregar").prop('disabled', false);
	else
		$("#agregar").prop('disabled', true);
}

$(document).ready(function() {
	$("#agregar").click(function() {
		// Metodo POST con Ajax para cargar la correlativa con los datos ingresados
		$.post("/correlativas/agregar_correlativa", {
			materia_id: $("#materia").find(":selected").val(),
			correlativa_id: $("#correlativa").find(":selected").val()
		}, function() {
			// Se carga el modal de exito
			$("#successModal").toggleClass("is-active");
			$("#successModal").toggleClass("is-clipped");
		});
	});

	// Cuando se seleccionan las materias se habilita el boton
	$("#materia, #correlativa").change(validar_inputs);

	$("#cerrar_modal").click(function() {
        $("#successModal").toggleClass("is-active");
        $("#successModal").toggleClass("is-clipped");
	});

});