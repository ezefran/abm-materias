// Valida que los inputs no esten vacios
var validar_inputs = function() {
	if($("#materia").find(":selected").val() != '' && $("#correlativa").find(":selected").val() != '')
		$("#modificar").prop('disabled', false);
	else
		$("#modificar").prop('disabled', true);
}

$(document).ready(function() {
	$("#modificar").click(function() {
		// Metodo POST con Ajax para cargar la correlativa con los datos ingresados
		$.post("/correlativas/modificar_correlativa", {
			id: $(location).attr('search').split('=')[1],
			materia_id: $("#materia").find(":selected").val(),
			correlativa_id: $("#correlativa").find(":selected").val()
		}, function() {
			// Se carga el modal de exito
			$("#successModal").addClass("is-active");
			$("#successModal").addClass("is-clipped");
		});
	});

	// Cuando se seleccionan las materias se habilita el boton
	$("#materia, #correlativa").change(validar_inputs);
	
	$("#cerrar_modal").click(function() {
        $("#successModal").removeClass("is-active");
		$("#successModal").removeClass("is-clipped");
	});

});