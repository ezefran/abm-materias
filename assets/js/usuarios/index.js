var pagina, search;

seleccionarPagina = function() {
	$("ul.pagination li a").click(function() {
		var pagina = $(this).text();
		Utils.seleccionarPagina("usuarios", pagina, busqueda);
	});
}

$(document).ready(function() {
	// Se ejecuta cuando se escribe en el input de busqueda
	$("#search").keyup(function() {
		busqueda = $(this).val()
		Utils.filtrarLista("usuarios", pagina, busqueda);
	})
	
	// Se ejecuta cuando se hace click en el boton Eliminar en el modal
	$("#eliminar_confirmado").click(function() {
		Utils.eliminarElemento("usuarios", $(this).val())
	});

	// Se ejecuta cuando se hace click en uno de los botones de paginacion
	seleccionarPagina();

	$("#deleteModal").click(function() {
		$(this).toggleClass("is-active");
	});

	/* Se ejecuta despues de cada request Ajax.
	Permite seleccionar elementos del nuevo html generado. */
	$(document).ajaxStop(function() {
		seleccionarPagina();
	});
});

// Se ejecuta cuando se hace click sobre el boton Eliminar que esta al lado de una usuario
function borrar(id) {
	// Modal para confirmar la eliminacion
	$(".modal-body").html('<h4>¿Estás seguro de que querés eliminar este usuario?</h4>');
	$("#eliminar_confirmado").val(id);
	$("#deleteModal").toggleClass("is-active");
};