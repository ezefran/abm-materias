$(document).ready(function() {
    $(".navbar-burger").click(function() {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
  
    });
    
    $('#logout').click(function() {
        $.post('/logout', {}, function() {window.location.href = "/login";});
    });
});