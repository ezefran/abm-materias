var regexUsuario = new RegExp('[^A-Za-z]');
var regexContrasena = new RegExp('[^A-Za-z0-9]');

var validar_inputs = function() {
	if ($("#usuario").val().trim() != '' && $("#contrasena").val().trim() != ''
    && $("#nombre").val().trim() != '' && $("#apellido").val().trim() != ''
    && $("#carrera").find(":selected").val() != '' && !regexContrasena.test($("#contrasena").val().trim())
    && !regexUsuario.test($("#usuario").val().trim() + $("#nombre").val().trim()
    + $("#apellido").val().trim()) && $("#tipo").find(":selected").val() != '')
		$("#agregar").prop('disabled', false);
	else
		$("#agregar").prop('disabled', true);
}

$(document).ready(function() {
    
    $("#agregar").click(function() {
        $.post("/usuarios/agregar_usuario", {
            usuario: $("#usuario").val().trim(),
            hash: $("#contrasena").val().trim(),
            nombre: $("#nombre").val().trim(),
            apellido: $("#apellido").val().trim(),
            carrera_id: $("#carrera").find(":selected").val(),
            tipo: $("#tipo").find(":selected").val(),
            estado: 1
        }, function() {
			$("#successModal").toggleClass("is-active");
			$("#successModal").toggleClass("is-clipped");
        });
    });

    // Validaciones de inputs en tiempo real:
	$("#usuario, #apellido, #nombre").keyup(function() {
		var nombre = $(this).val().trim();
		if(regexUsuario.test(nombre)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});

    $("#contrasena").keyup(function() {
		var contrasena = $(this).val().trim();
		if(regexContrasena.test(contrasena)) {
			$(this).addClass('is-danger');
			$("#agregar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			validar_inputs();
		}
	});

    // Cuando cambia cualquier input se valida que los campos no esten vacios
	$("#carrera").change(validar_inputs);
	$("#tipo").change(validar_inputs);

	$("#cerrar_modal").click(function() {
        $("#successModal").toggleClass("is-active");
        $("#successModal").toggleClass("is-clipped");
	});

});