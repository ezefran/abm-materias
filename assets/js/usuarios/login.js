var regexUsuario = new RegExp('[^A-Za-z]');
var regexContrasena = new RegExp('[^A-Za-z0-9]');

var validar_inputs = function() {
	if ($("#reg_usuario").val().trim() != '' && $("#reg_contrasena").val().trim() != ''
    && $("#reg_nombre").val().trim() != '' && $("#reg_apellido").val().trim() != ''
    && $("#reg_carrera").find(":selected").val() != '' && !regexContrasena.test($("#reg_contrasena").val().trim())
    && !regexUsuario.test($("#reg_usuario").val().trim() + $("#reg_nombre").val().trim()
    + $("#reg_apellido").val().trim()))
		$("#registrar").prop('disabled', false);
	else
		$("#registrar").prop('disabled', true);
}

$(document).ready(function() {
    $("#login").click(function() {
        var usuario = $("#usuario").val().trim();
        var contrasena = $("#contrasena").val().trim();
        var titulo, mensaje, error = false;

        if (regexUsuario.test(usuario) || regexContrasena.test(contrasena)) {
            titulo = "Campos inválidos";
            mensaje = "Solo se permite usar letras en el nombre de usuario y letras y números en la contraseña.";
            error = true;
        }
        else if (usuario == '' || contrasena == '') {
            titulo = "Campos vacíos";
            mensaje = "Debes ingresar el nombre usuario y la contraseña.";
            error = true;
        }

        if (error) {
            show_modal(titulo, mensaje)
            return;
        }

        $.post("/usuarios/validar_login", {
            usuario: usuario,
            contrasena: contrasena
        }, function(data) {
            switch(data) {
                case "ERROR_USUARIO_NO_EXISTE":
                    titulo = "Usuario inexistente";
                    mensaje = "El nombre de usuario ingresado no se encuentra registrado.";
                    show_modal(titulo, mensaje);
                    break;
                case "ERROR_USUARIO_INACTIVO":
                    titulo = "Usuario inactivo";
                    mensaje = "El usuario ingresado se encuentra inactivo.";
                    show_modal(titulo, mensaje);
                    break;
                case "ERROR_CONTRASENA_INCORRECTA":
                    titulo = "Contraseña incorrecta";
                    mensaje = "La contraseña ingresada es incorrecta";
                    show_modal(titulo, mensaje);
                    break;
                default:
                    window.location.href = "/login";
            }
        });

    });

    // Cuando se apreta enter en un input del login
    $("#usuario, #contrasena").keyup(function(event) {
        if(event.keyCode == 13) {
            $("#login").click();
        }
    });

    // Cuando se hace click en el boton de registro
    $("#registrar_init").click(function() {
        $("#modal_registro").toggleClass("is-active");
        $("#modal_registro").toggleClass("is-clipped");
    });

    $("#registrar").click(function() {
        $.post("/usuarios/registrar_alumno", {
            usuario: $("#reg_usuario").val().trim(),
            hash: $("#reg_contrasena").val().trim(),
            nombre: $("#reg_nombre").val().trim(),
            apellido: $("#reg_apellido").val().trim(),
            carrera_id: $("#reg_carrera").find(":selected").val()
        }, function() {
            titulo = "Registro exitoso";
            mensaje = "Te has registrado correctamente.";
            $("#modal_registro").toggleClass("is-active");
            $("#modal_registro").toggleClass("is-clipped");
            show_modal(titulo, mensaje);
        });
    });

    // Validaciones de inputs de registro en tiempo real:
	$("#reg_usuario, #reg_apellido, #reg_nombre").keyup(function() {
		var nombre = $(this).val().trim();
		if(regexUsuario.test(nombre)) {
			$(this).addClass('is-danger');
			$(this).parent().parent().children(".help").removeClass("hidden");
			$("#registrar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			$(this).parent().parent().children(".help").addClass("hidden");
			validar_inputs();
		}
	});

    $("#reg_contrasena").keyup(function() {
		var contrasena = $(this).val().trim();
		if(regexContrasena.test(contrasena)) {
			$(this).addClass('is-danger');
			$(this).parent().parent().children(".help").removeClass("hidden");
			$("#registrar").prop('disabled', true);
		}
		else {
			$(this).removeClass('is-danger');
			$(this).parent().parent().children(".help").addClass("hidden");
			validar_inputs();
		}
	});

    // Cuando cambia cualquier input del registro se valida que los campos no esten vacios
	$("#reg_carrera").change(validar_inputs);

    $("#cerrar_modal").click(function() {
        $("#modal").toggleClass("is-active");
        $("#modal").toggleClass("is-clipped");
    });

    $("#cerrar_modal_registro").click(function() {
        $("#modal_registro").toggleClass("is-active");
        $("#modal_registro").toggleClass("is-clipped");
    });
});

function show_modal(titulo, mensaje) {
    $(".modal-title").html(titulo);
    $("#modal_text").html(mensaje);
    $("#modal").toggleClass("is-active");
    $("#modal").toggleClass("is-clipped");
}