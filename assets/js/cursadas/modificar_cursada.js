$(document).ready(function() {
	$("#modificar").click(function() {
		var id = $(location).attr('search').split('=')[1];
		var materia_id = $("#materia").data('materia');
		var usuario_id = $("#usuario").data('usuario');
		var estado = $("#estado").val();
		var nota = $("#nota").find(":selected").val();
		var fecha = $("#fecha").val();
		
		// Metodo POST con Ajax para actualizar la materia con los datos ingresados
		$.post("/cursadas/modificar_cursada", {
			id: id,
			materia_id: materia_id,
			usuario_id: usuario_id,
			estado: estado,
			nota: nota,
			fecha: fecha
		}, function() {
			// Se carga el modal de exito
			$("#successModal").toggleClass("is-active");
			$("#successModal").toggleClass("is-clipped");
		});
		
	});

	// Cuando se selecciona el estado se mustra el selector de fecha y nota o no
	$("#estado").change(function() {
		var estado = $(this).val();
		if (estado != 'EN CURSO' && estado != 'ABANDONO' && estado != '') {
			$("#hay_estado").show();
			$("#modificar").prop('disabled', true);
			$("#fecha").val('');
			$("#nota").val('');
		}
		else if (estado != '') {
			$("#hay_estado").hide();
			$("#modificar").prop('disabled', false);
			$("#fecha").val('');
			$("#nota").val('');
		}
		else {
			$("#hay_estado").hide();
			$("#modificar").prop('disabled', true);
		}
	});

	$("#nota").change(function() {
		if($("#fecha").val() != '')
			$("#modificar").prop('disabled', false);
	});

	$("#fecha").change(function() {
		if($("#nota").val() != '')
			$("#modificar").prop('disabled', false);
	});

	$("#cerrar_modal").click(function() {
        $("#successModal").toggleClass("is-active");
        $("#successModal").toggleClass("is-clipped");
	});
	
});