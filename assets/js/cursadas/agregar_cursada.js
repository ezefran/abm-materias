$(document).ready(function() {
	$("#agregar").click(function() {
		var materia_id = $("#materia").find(":selected").val();
		var usuario_id = $("#usuario").find(":selected").val();
		var estado = $("#estado").val();
		var nota = $("#nota").find(":selected").val();
		var fecha = $("#fecha").val();
		
		// Metodo POST con Ajax para cargar la materia con los datos ingresados
		if(materia_id != '' && usuario_id != '' && estado != '' ||
		materia_id != '' && usuario_id != '' && estado != '' && nota != '' && fecha != '') {
			$.post("/cursadas/agregar_cursada", {
				materia_id: materia_id,
				usuario_id: usuario_id,
				estado: estado,
				nota: nota,
				fecha: fecha
			}, function() {
				// Se carga el modal de exito
				$("#successModal").toggleClass("is-active");
				$("#successModal").toggleClass("is-clipped");
			});
		}
	});

	// Cuando se selecciona el usuario se muestran las materias
	$("#usuario").change(function() {
		$.post("/cursadas/agregar_cursada", {
			usuario_id: $("#usuario option:selected").val()
		}, function(data) {
			if (data != 'NO_USUARIO') {
				$("#div-materia").show();
				$("#materia").html(data);
			}
			else {
				$("#div-materia").hide();
				$("#hay_materia").hide();
			}
		});
	});

	// Cuando se selecciona la materia se muestran los estados
	$("#materia").change(function() {
		if ($(this).val() != '')
			$("#hay_materia").show();
		else
			$("#hay_materia").hide();
	});

	// Cuando se selecciona el estado se mustra el selector de fecha y nota o el boton
	$("#estado").change(function() {
		var estado = $(this).val();
		if (estado != 'EN CURSO' && estado != 'ABANDONO' && estado != '') {
			$("#hay_estado").show();
			$("#agregar").prop('disabled', true);
			$("#fecha").val('');
			$("#nota").val('');
		}
		else if (estado != '') {
			$("#hay_estado").hide();
			$("#agregar").prop('disabled', false);
			$("#fecha").val('');
			$("#nota").val('');
		}
		else {
			$("#hay_estado").hide();
			$("#agregar").prop('disabled', true);
		}
	});

	// Cuando se seleccionan la nota y la fecha se habilita el boton
	$("#nota").change(function() {
		if($("#fecha").val() != '')
			$("#agregar").prop('disabled', false);
	});

	$("#fecha").change(function() {
		if($("#nota").val() != '')
			$("#agregar").prop('disabled', false);
	});

	$("#cerrar_modal").click(function() {
        $("#successModal").toggleClass("is-active");
        $("#successModal").toggleClass("is-clipped");
	});

});