USE curso_php;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `carreras` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Arquitectura', 'Es la descripcion de Arquitectura'),
(2, 'Ingenieria Mecanica', 'Es la descripcion de Ingenieria Mecanica'),
(3, 'Ingenieria en Informatica', 'Es la descripcion de Ingenieria en Informatica'),
(4, 'Ingenieria en Electronica', 'Es la descripcion de InIngenieria en Electronica'),
(5, 'Ingenieria Industrial', 'Es la descripcion de Ingenieria Industrial'),
(6, 'Ingenieria Civil', 'Es la descripcion de Ingenieria Civil'),
(7, 'Desarrollo Web', 'Es la descripcion de Desarrollo Web'),
(8, 'Sonido y Grabacion', 'Es la descripcion de Sonido y Grabacion');

INSERT INTO `materias` (`id`, `carrera_id`, `nombre`, `descripcion`, `carga_horaria`) VALUES
(1, 7, 'Base de Datos II', 'Introduccion al paradigma relacional de bases de datos y al lenguaje SQL', 4),
(2, 7, 'Taller Web I', 'Desarrollo de aplicaciones Web usando Java, Hibernate y Spring MVC', 4),
(3, 3, 'Analisis de Sistemas', 'Descripcion de Analisis de Sistemas', 10),
(4, 3, 'Diseño de Sistemas', 'Descripcion de Diseño de Sistemas', 10),
(5, 1, 'Matematica Aplicada I', 'Descripcion de Matematica Aplicada I', 4),
(6, 1, 'Forma I', 'Descripcion de Forma I', 4),
(7, 4, 'Introduccion a los Sistemas Digitales', 'Descripcion de Introduccion a los Sistemas Digitales', 4),
(8, 4, 'Tecnicas Digitales I', 'Descripcion de Tecnicas Digitales I', 4),
(9, 5, 'Empresa y sus Estructuras', 'Descripcion de Empresa y sus Estructuras', 4),
(10, 5, 'Elementos de Economia', 'Descripcion de Elementos de Economia', 4),
(11, 6, 'Estabilidad', 'Descripcion de Estabilidad', 8),
(12, 6, 'Materiales de Construccion', 'Descripcion de Materiales de Construccion', 8),
(13, 2, 'Metalurgia Fisica I', 'Descripcion de Metalurgia Fisica I', 4),
(14, 2, 'Metalurgia Fisica II', 'Descripcion de Metalurgia Fisica II', 4),
(15, 8, 'Acustica Fisica', 'Descripcion de Acustica Fisica', 8),
(16, 8, 'Conceptos Ritmicos', 'Descripcion de Conceptos Ritmicos', 4),
(17, 3, 'Analisis Matematico II', 'Descripcion de Analisis Matematico II', 8),
(18, 3, 'Analisis Matematico I', 'Descripcion de Analisis Matematico I', 8);

INSERT INTO `correlativas` (`id`, `materia_id`, `correlativa_id`) VALUES
(1, 4, 3),
(2, 14, 13),
(3, 17, 18);

INSERT INTO `usuarios` (`id`, `usuario`, `carrera_id`, `nombre`, `apellido`, `hash`, `tipo`, `estado`, `fechaAlta`) VALUES
(1, 'admin', 1, 'Admin', 'Admin', '$2a$10$mEjYkHC/08WqezA/HGGNWeNVia..UqItvHgVSjgx0p360R9y6gmIu', 0, 1, '2017-04-10'),
(2, 'alumno', 3, 'NombreAlumno', 'ApellidoAlumno', '$2a$10$mEjYkHC/08WqezA/HGGNWeNVia..UqItvHgVSjgx0p360R9y6gmIu', 1, 1, '2017-04-10');

INSERT INTO `cursadas` (`id`, `usuario_id`, `materia_id`, `nota`, `fecha`, `estado`) VALUES
(1, 2, 4, 2, '2017-01-01', 'EN CURSO'),
(2, 2, 3, 6, '2015-06-27', 'FINAL');
