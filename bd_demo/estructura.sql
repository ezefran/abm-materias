CREATE DATABASE curso_php;
USE curso_php;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `carreras` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `correlativas` (
  `id` int(11) NOT NULL,
  `materia_id` int(11) NOT NULL,
  `correlativa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `cursadas` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `materia_id` int(11) NOT NULL,
  `nota` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `materias` (
  `id` int(11) NOT NULL,
  `carrera_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `carga_horaria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE TRIGGER `fix_carga_horaria` BEFORE INSERT ON `materias` FOR EACH ROW IF (NEW.carga_horaria NOT IN (2,4,6,8,10)) THEN
	SET NEW.carga_horaria = 2;
END IF
$$
DELIMITER ;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `carrera_id` int(11) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechaAlta` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE TRIGGER `set_fechaAlta` BEFORE INSERT ON `usuarios` FOR EACH ROW SET NEW.fechaAlta = CURDATE()
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `correlativas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_materias_correlativas.materia` (`materia_id`),
  ADD KEY `fk_materias_correlativas.correlativa` (`correlativa_id`);

ALTER TABLE `cursadas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `materias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuarios_carreras` (`carrera_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

ALTER TABLE `carreras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `correlativas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `cursadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `materias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

ALTER TABLE `correlativas`
  ADD CONSTRAINT `fk_materias_correlativas.correlativa` FOREIGN KEY (`correlativa_id`) REFERENCES `materias` (`id`),
  ADD CONSTRAINT `fk_materias_correlativas.materia` FOREIGN KEY (`materia_id`) REFERENCES `materias` (`id`);

ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_carreras` FOREIGN KEY (`carrera_id`) REFERENCES `carreras` (`id`);
